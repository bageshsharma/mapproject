package test.map.com.mapproject.view.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_pet_listing.*
import test.map.com.mapproject.R
import test.map.com.mapproject.view.adapter.PetListingAdapter
import test.map.com.mapproject.view.model.PetDataModel

class PetListingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pet_listing)
        initializeActionBar()
        initializeAdapter()
    }

    fun initializeActionBar() {
        setSupportActionBar(petListingToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    fun initializeAdapter() {
        val petData: List<PetDataModel> = listOf(PetDataModel(1, "", 2, "BLACK", 1, "9999577058", "", "Manauna", "Bareilly", 23.455, 23.44, 1), PetDataModel(2, "", 2, "BLACK", 1, "9999577058", "", "Aonla", "Bareilly", 23.455, 23.44, 1))
        petListingRecyclerView.layoutManager = LinearLayoutManager(this)
        petListingRecyclerView.adapter = PetListingAdapter(petData)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            this.finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}
