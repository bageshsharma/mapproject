package test.map.com.mapproject.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.ToggleButton
import kotlinx.android.synthetic.main.pet_item_layout.view.*
import test.map.com.mapproject.R
import test.map.com.mapproject.view.model.PetDataModel

/**
 * Created by BDNQ4535 on 2017-08-19.
 */

class PetListingAdapter(private val mPetDataList: List<PetDataModel>?) : RecyclerView.Adapter<PetListingAdapter.PetListingViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PetListingViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.pet_item_layout, parent, false)
        return PetListingViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: PetListingViewHolder, position: Int) {
        holder.petAddressTextView.text = mPetDataList!![position].palace
        holder.petSellingToggle.isChecked = mPetDataList[position].status == 1
    }

    override fun getItemCount(): Int = mPetDataList?.size ?: 0

    inner class PetListingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var petIconImageView: ImageView
        var petAddressTextView: TextView
        var petSellingToggle: ToggleButton

        init {
            petIconImageView = itemView.petIconImageView
            petAddressTextView = itemView.petAddressTextView
            petSellingToggle = itemView.petSellingToggle
        }
    }
}
