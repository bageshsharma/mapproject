package test.map.com.mapproject.view.model

/**
 * Created by BDNQ4535 on 2017-08-19.
 */

data class PetDataModel(val petId: Int, val petImageUrl: String, val petType: Int, val petColor: String?, val userId: Long, val mobile: String?, val email: String?, val address: String?, val palace: String?, val latitude: Double, val longitude: Double, val status: Int)